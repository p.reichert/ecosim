#################################
# create package
#################################


# generate a package skeleton:
#package.skeleton("ecosim",code_files="ecosim.r")

# check package:
system("R CMD check ecosim")

# build source package (.tar.gz):
system("R CMD build ecosim")

# check processed package:
system("R CMD check --as-cran ecosim_1.3-4.tar.gz")

install.packages("ecosim_1.3-4.tar.gz",repos=NULL,type="source")
library(ecosim)
help(ecosim)


# upload:
# https://cran.r-project.org/submit.html



